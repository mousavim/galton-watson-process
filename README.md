# Galton–Watson process (Disease transmission to future generations)

Simulation of Galton-Watson process which is a type of Branching Markov Chain.

The problem is equivalent to the problem of passing the family-name. In a society where the family-name is passed from father to children, what is the probability of having the family-name of a father in the _N_-th generation after him.

Similarly, the problem can be seen as the probability of disease (or gene) transmission over the generations where a disease is transferred from a father to its children. What is the probability that at least one person in the _N_-th generation have the disease? 

In this code, it is assumed that the probability of a child being boy or girl is 0.5.



Details on mathematical modeling and the solution to this problem can be found here:
https://math.stackexchange.com/questions/3970708/probability-of-disease-transmission-to-future-generations

Further study:
https://en.wikipedia.org/wiki/Galton%E2%80%93Watson_process
