import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from operator import mul  
from fractions import Fraction
from functools import reduce

#======================== Functions ======================================
def make_family(boys_list, generation, max_children, max_generation, case_found):
    
    generation=generation+1
    children = np.random.randint(0,2,max_children)
    for f in children:
        boys_list = boys_list[0:generation]
        if not case_found and f==1:
            boys_list.append(1)
            if len(boys_list)==max_generation:
                case_found=1
                break

            case_found = make_family(boys_list, generation, max_children, max_generation, case_found)

    return case_found


def n_Choose_k(max_children,k): 
  return int( reduce(mul, (Fraction(max_children-i, i+1) for i in range(k)), 1) )

#========================== Main =========================================
if __name__ == "__main__": 
    # Parameter setting
    max_children=list(range(1,5))      # number of children per family
    max_generation=list(range(2,15))    # maximum number of generations to be considered
    TOTAL_TEST=5000

    prob_simulation=pd.DataFrame(columns=max_children)
    prob_simulation.insert(0, '(row,col)=(#gnr, #chld)', max_generation)
    prob_theory=prob_simulation.copy()

    print('---------------------- Simulation started -------------------------')
    print('Number of children: \t {} to {}'.format(max_children[0], max_children[-1]))
    print('Number of generations: \t {} to {}'.format(max_generation[0], max_generation[-1]))

    c_count=0
    g_count=0
    for c in max_children:
        for g in max_generation:

            # Simulation -------------------------------------------------------
            true_case=0
            n_test=0
            while n_test<TOTAL_TEST:
                n_test+=1
                boys_list=[1]
                generation=0
                case_found=0
                is_case = make_family(boys_list, generation, c, g, case_found)

                if is_case:
                    true_case+=1

            filt= prob_simulation['(row,col)=(#gnr, #chld)']==g
            prob_simulation.loc[filt, c] = true_case/TOTAL_TEST

            # Theory ------------------------------------------------------------
            q=[0]
            q.append(0)
            q.append(1/2**c)
            for n in range(3,g+1):
                #print(n)
                sum=0
                for k in range(c+1):
                    #print(k)
                    sum= sum+ n_Choose_k(c,k)*q[n-1]**k

                q.append((1/2**c)*sum)

            prob_theory.loc[filt, c]=1-q[-1]


    #========================== Plot ========================================= 
    n_generation = prob_simulation['(row,col)=(#gnr, #chld)'].tolist()

    fig, ax = plt.subplots()
    cmap = plt.get_cmap('jet')
    for i, n_child in enumerate(max_children):
        color = cmap(float(i)/len(max_children))

        p_sim=prob_simulation[n_child]
        ax.plot(n_generation, p_sim, c=color, label='(Simul) Chil. of parents: {}'.format(n_child))

        p_theo=prob_theory[n_child]
        ax.plot(n_generation, p_theo, c=color, linestyle='--', marker='>', label='(Theo) Chil. of parents: {}'.format(n_child))
        
    ax.legend(loc='upper right')
    ax.set(xlabel='Max. no. of generations', ylabel='Probability',
        title='Probability of disease transmission to future generations')
    ax.grid()
    plt.show()

